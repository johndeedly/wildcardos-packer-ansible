#!/usr/bin/env bash

# https://www.enricozini.org/blog/2021/debian/nspawn-chroot-maintenance/
chroot="$1"
shift
exec systemd-nspawn --capability=CAP_SYS_CHROOT,CAP_NET_ADMIN,CAP_NET_RAW --resolv-conf=bind-host --link-journal=host --console=pipe -qD "$chroot" -- "$@"
