#!/usr/bin/env bash

# i18n
sed -i 's/^#\?de_DE.UTF-8 UTF-8/de_DE.UTF-8 UTF-8/' /etc/locale.gen
locale-gen
echo "LANG=de_DE.UTF-8" > /etc/locale.conf
rm /etc/localtime || true
ln -s /usr/share/zoneinfo/CET /etc/localtime
tee /etc/vconsole.conf <<EOF
KEYMAP=de-latin1
XKBLAYOUT=de
XKBMODEL=pc105
EOF
loadkeys de-latin1 || true
