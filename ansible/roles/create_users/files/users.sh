#!/usr/bin/env bash

# read administrative database to get usernames, group names and home directories
ROOTID=$(getent passwd 0 | cut -d: -f1)
ROOTGRP=$(getent group "$(getent passwd 0 | cut -d: -f4)" | cut -d: -f1)
ROOTHOME=$(getent passwd 0 | cut -d: -f6)
USERID=$(getent passwd 1000 | cut -d: -f1)
USERGRP=$(getent group "$(getent passwd 1000 | cut -d: -f4)" | cut -d: -f1)
USERHOME=$(getent passwd 1000 | cut -d: -f6)
TEMPID=$(getent passwd 65534 | cut -d: -f1)
TEMPGRP=$(getent group "$(getent passwd 65534 | cut -d: -f4)" | cut -d: -f1)
TEMPHOME=$(getent passwd 65534 | cut -d: -f6)

# make sure temp user has a temporary writable home
if [ -n "${TEMPHOME}" ] && [ -e "${TEMPHOME}" ]; then
  statU="$(stat -c '%U' "$1")"
  if [ "x${statU}" != "x${TEMPID}" ]; then
    TEMPHOME="/var/tmp"
  fi
else
  TEMPHOME="/var/tmp"
fi

echo -en "${ROOTID}\n${ROOTGRP}\n${ROOTHOME}\n"
echo -en "${USERID}\n${USERGRP}\n${USERHOME}\n"
echo -en "${TEMPID}\n${TEMPGRP}\n${TEMPHOME}\n"
