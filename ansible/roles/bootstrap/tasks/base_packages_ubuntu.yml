# ---------- #

- name: Add package source for dotnet
  shell: |
    REPO_VERSION=$(if command -v lsb_release &> /dev/null; then lsb_release -r -s; else grep -oP '(?<=^VERSION_ID=).+' /etc/os-release | tr -d '"'; fi)
    wget "https://packages.microsoft.com/config/ubuntu/$REPO_VERSION/packages-microsoft-prod.deb" -O packages-microsoft-prod.deb
    dpkg -i packages-microsoft-prod.deb
    rm packages-microsoft-prod.deb
    apt update

- name: Install and configure base packages needed for everything else
  shell: |
    eatmydata apt install -y {{ item | join(" ") }}
    sync
  retries: 5
  delay: 10
  loop:
    # ease of use
    - [ nano, neovim, htop, btop, dialog, git, bash-completion, ncdu, rustc, cargo, pv, mc, lfm, fzf ]
    # parsing tools
    - [ lshw, libxml2, jq ]
    # security
    - [ policykit-1, man, manpages-de, trash-cli ]
    # dotnet
    - [ dotnet-sdk-8.0, dotnet-runtime-8.0, aspnetcore-runtime-8.0, dotnet-sdk-6.0, dotnet-runtime-6.0, aspnetcore-runtime-6.0 ]
    # python, words for xkcd
    - [ python-is-python3, python3-pip, wngerman, python3-setuptools, python3-wheel ]
    # network
    - [ openssh-server, openssh-client, ufw, wireguard-tools, wget ]
    # filesystem
    - [ gvfs, gvfs-backends, sshfs, cifs-utils, nfs-kernel-server ]
    # packaging
    - [ unzip,  p7zip, rsync ]
    # integration
    - [ xdg-user-dirs, xdg-utils ]

- name: Install and configure ly
  shell: |
    eatmydata apt install -y cmake libpam0g-dev libx11-xcb-dev
    sync
    pushd /var/tmp
      mkdir ly
      chown nobody:nogroup ly
      chmod u=rwx,g-rwx,o-rwx,u+s,g+s,o+t ly
    popd
    su -s /bin/bash - nobody <<EOF
    pushd /var/tmp/ly
      git clone --recurse-submodules https://github.com/fairyglade/ly .
      make
    popd
    EOF
    pushd /var/tmp/ly
      make install installsystemd
    popd
    rm -rf /var/tmp/ly

- name: Install and configure viu
  shell: |
    cargo install viu --locked
  environment:
    CARGO_TARGET_DIR: /var/tmp
    CARGO_INSTALL_ROOT: /usr/local

- name: Install and configure starship
  shell: |
    cargo install starship --locked
  environment:
    CARGO_TARGET_DIR: /var/tmp
    CARGO_INSTALL_ROOT: /usr/local

- name: Enable basic services
  systemd_service:
    enabled: true
    name: "{{ item }}"
  loop:
    - systemd-networkd
    - systemd-resolved
    - systemd-homed
    - ssh
    - ufw
    - ly

- name: Disable services for tty login
  systemd_service:
    name: "{{ item }}"
    masked: true
  loop:
    - console-getty.service
    - getty@tty2.service

- name: Ensure the /etc/ly directory exists
  file:
    path: /etc/ly
    state: directory

- name: Configure ly to have username and shell prefilled on first boot
  copy:
    dest: /etc/ly/save
    content: |
      {{ create_users_user_id }}
      0

- name: Install xkcd for user, root and skeleton
  shell: |
    PYTHONUSERBASE="{{ item[1] }}" python -m pip install --user --break-system-packages --no-warn-script-location xkcdpass || true
  become: true
  become_method: su
  become_user: "{{ item[0] }}"
  become_flags: "-s /bin/bash -l"
  loop:
    - [ "{{ create_users_user_id }}", "{{ create_users_user_home }}/.local" ]
    - [ "{{ create_users_root_id }}", "{{ create_users_root_home }}/.local" ]
    - [ "{{ create_users_root_id }}", "/etc/skel/.local" ]

- name: Install PowerShell
  shell: |
    dotnet tool install --global PowerShell --version 7.4.0
  become: true
  become_method: su
  become_user: "{{ item }}"
  become_flags: "-s /bin/bash -l"
  retries: 5
  delay: 10
  loop:
    - "{{ create_users_root_id }}"
    - "{{ create_users_user_id }}"
