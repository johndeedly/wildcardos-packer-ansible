#!/usr/bin/env bash
#
# /usr/bin/usersleep.sh
# https://unix.stackexchange.com/a/713646
# https://wiki.archlinux.org/title/Power_management#Hooks_in_/usr/lib/systemd/system-sleep
#

case $1 in
  pre)
    # execute as logged in users
    for user in $(loginctl --no-legend list-sessions | awk '{print $3}'); do
      /usr/bin/systemctl --user --machine="${user}@" start --wait sleep.target
    done
    unset user
    # execute as root user
    sleep 1
    ;;
  post)
    # execute as logged in users
    for user in $(loginctl --no-legend list-sessions | awk '{print $3}'); do
      /usr/bin/systemctl --user --machine="${user}@" start --wait resume.target
    done
    unset user
    # execute as root user
    sleep 1
    /usr/bin/systemctl restart bluetooth iwd networkd-dispatcher
    ;;
esac
