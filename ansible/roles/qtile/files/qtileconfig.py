# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
# Copyright (c) 2023 johndeedly
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import subprocess
import glob
import os
import random
from libqtile import qtile, bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, KeyChord, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import send_notification
if qtile.core.name == "x11":
    from Xlib import display
from qtile_extras.popup.toolkit import PopupGridLayout, PopupText

# ==========
# Keys
# ==========

mod = "mod4"
alt_mod = "mod1"
shift = "shift"
ctrl = "control"
terminal = "kitty"
menu = "wofi --fork --normal-window --insensitive --allow-images --allow-markup --show drun"
lock = "xautolock -locknow"
filemanager = "pcmanfm"
filemanager2 = "kitty /usr/bin/lf"
webbrowser = "firefox --new-window"
webbrowser2 = "chromium"
output_config = "arandr"
screenshot = "flameshot gui"
suspend = "systemctl suspend-then-hibernate"
reboot = "systemctl reboot"
poweroff = "systemctl poweroff"
vol_up = "pamixer -i 5"
vol_down = "pamixer -d 5"
vol_mute = "pamixer -t"
vol_mic_mute = "pamixer --default-source -t"
vol_play = "playerctl play-pause"
vol_stop = "playerctl stop"
vol_next = "playerctl next"
vol_prev = "playerctl prev"
brightness_up = "brightnessctl -q s +10%"
brightness_down = "brightnessctl -q s 10%-"


def move_to_prev_screen(self) -> None:
    """Move the current window to the previous screen"""
    if qtile.current_window:
        to_screen = (qtile.screens.index(
            qtile.current_screen) - 1) % len(qtile.screens)
        to_group = qtile.screens[to_screen].group.name
        qtile.current_window.togroup(to_group)
        #qtile.focus_screen(to_screen)


def move_to_next_screen(self) -> None:
    """Move the current window to the next screen"""
    if qtile.current_window:
        to_screen = (qtile.screens.index(
            qtile.current_screen) + 1) % len(qtile.screens)
        to_group = qtile.screens[to_screen].group.name
        qtile.current_window.togroup(to_group)
        #qtile.focus_screen(to_screen)


keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod, shift], "Return", lazy.spawn(menu), desc="Launch menu"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([mod], "l", lazy.spawn(lock), desc="Locks the screen"),
    Key([mod], "e", lazy.spawn(filemanager), desc="File explorer"),
    Key([alt_mod], "e", lazy.spawn(filemanager2), desc="Shell file explorer"),
    Key([mod], "w", lazy.spawn(webbrowser), desc="Spawns firefox"),
    Key([alt_mod], "w", lazy.spawn(webbrowser2), desc="Spawns chromium"),
    Key([mod], "p", lazy.spawn(output_config), desc="Output configuration"),
    Key([mod], "Print", lazy.spawn(screenshot), desc="Takes a screenshot"),
    Key([mod, shift], "r", lazy.reload_config(), desc="Reload qtile config"),
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "f", lazy.window.toggle_fullscreen(),
        desc="Toggle fullscreen"),
    Key([mod], "m", lazy.window.toggle_floating(),
        desc="Toggle floating"),
    # Switch between windows
    Key([mod], "Left", lazy.layout.left(), desc="Move focus left"),
    Key([mod], "Right", lazy.layout.right(), desc="Move focus right"),
    Key([mod], "Down", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "Up", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(),
        desc="Move focus to next window"),
    # Switch between screens
    Key([alt_mod], "Left", lazy.prev_screen(),
        desc="Move focus to prev screen"),
    Key([alt_mod], "Right", lazy.next_screen(),
        desc="Move focus to next screen"),
    # Move windows around
    Key([mod, shift], "Left", lazy.layout.swap_left()
        (), desc="Swap window left"),
    Key([mod, shift], "Right", lazy.layout.swap_right(),
        desc="Swap window right"),
    Key([mod, shift], "Down", lazy.layout.shuffle_down(), desc="Swap window down"),
    Key([mod, shift], "Up", lazy.layout.shuffle_up(), desc="Swap window up"),
    # Move windows to other screen
    Key([alt_mod, shift], "Left", lazy.function(
        move_to_prev_screen), desc="Move window to prev screen"),
    Key([alt_mod, shift], "Right", lazy.function(
        move_to_next_screen), desc="Move window to next screen"),
    # Grow windows
    Key([mod, ctrl], "Left", lazy.layout.grow_left(), desc="Grow window left"),
    Key([mod, ctrl], "Right", lazy.layout.grow_right(), desc="Grow window right"),
    Key([mod, ctrl], "Down", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, ctrl], "Up", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod, ctrl], "Page_Up", lazy.layout.grow(), desc="Grow window on all sides"),
    Key([mod, ctrl], "Page_Down", lazy.layout.shrink(), desc="Shrink window on all sides"),
    Key([mod, ctrl], "Home", lazy.layout.reset(), desc="Reset layout"),
    # Bye-bye menu
    KeyChord([mod, shift], "q", [
        Key([], "l", lazy.shutdown(), desc="Logout"),
        Key([], "s", lazy.spawn(suspend), desc="Suspend"),
        Key([], "r", lazy.spawn(reboot), desc="Reboot"),
        Key([], "p", lazy.spawn(poweroff), desc="Poweroff"),
        Key([], "Return", lazy.ungrab_chord(), desc="Abort"),
        Key([], "Escape", lazy.ungrab_chord(), desc="Abort")],
        name="Bye-bye mode: (l)ogout, (s)uspend, (r)eboot, (p)oweroff, (return/esc) abort"
    ),
    # Video and audio controls
    Key([], "XF86AudioRaiseVolume", lazy.spawn(vol_up), desc="Raise Volume"),
    Key([], "XF86AudioLowerVolume", lazy.spawn(vol_down), desc="Lower Volume"),
    Key([], "XF86AudioMute", lazy.spawn(vol_mute), desc="Mute Audio"),
    Key([], "XF86AudioPlay", lazy.spawn(vol_play), desc="Play Audio"),
    Key([], "XF86AudioStop", lazy.spawn(vol_stop), desc="Stop Audio"),
    Key([], "XF86AudioNext", lazy.spawn(vol_next), desc="Play Next Track"),
    Key([], "XF86AudioPrev", lazy.spawn(vol_prev), desc="Play Previous Track"),
    Key([], "XF86MonBrightnessUp", lazy.spawn(
        brightness_up), desc="Brightness Up"),
    Key([], "XF86MonBrightnessDown", lazy.spawn(
        brightness_down), desc="Brightness Down")
]


@lazy.function
def show_power_menu(qtile, keys):
    controls = [
        PopupText(
            text='Qtile Keybindings',
            h_align="center",
            row=0,
            col=0,
            row_span=1,
            col_span=4
        ),
    ]
    keys_ignored = (
        "XF86AudioMute",  #
        "XF86AudioLowerVolume",  #
        "XF86AudioRaiseVolume",  #
        "XF86AudioPlay",  #
        "XF86AudioNext",  #
        "XF86AudioPrev",  #
        "XF86AudioStop",
        "XF86MonBrightnessUp",
        "XF86MonBrightnessDown",
    )
    text_replaced = {
        "mod4": "[SUPER]",  #
        "control": "[CTRL]",  #
        "mod1": "[ALT]",  #
        "shift": "[SHIFT]",  #
        "Escape": "ESC",  #
        "Return": "↩",  #
        "Left": "⬅",  #
        "Right": "➡",  #
        "Up": "⬆",  #
        "Down": "⬇",  #
        "Page_Up": "⤴", #
        "Page_Down": "⤵", #
    }
    rows = 1
    left = 0
    for key in keys:
        if key.key in keys_ignored:
            continue
        try:
            comb = ""
            if type(key) is Key:
                desc = key.desc.title()
            elif type(key) is KeyChord:
                desc = key.name.title()
            else:
                desc = ""
            for m in key.modifiers:
                if m in text_replaced.keys():
                    comb += text_replaced[m] + "+"
                else:
                    comb += m.capitalize() + "+"
            if len(key.key) > 1:
                if key.key in text_replaced.keys():
                    comb += text_replaced[key.key]
                else:
                    comb += key.key.title()
            else:
                comb += key.key
            controls.extend([
                PopupText(
                    text=comb+": ",
                    h_align="right",
                    row=rows,
                    col=left,
                    row_span=1,
                    col_span=1,
                    background=colors[rows % 2*2],
                ),
                PopupText(
                    text=desc,
                    h_align="left",
                    row=rows,
                    col=left+1,
                    row_span=1,
                    col_span=1,
                    background=colors[rows % 2*2],
                ),
            ])
            if left == 2:
                rows = rows + 1
            left = (left + 2) % 4
        except:
            pass

    layout = PopupGridLayout(
        qtile,
        width=800,
        height=600,
        rows=rows,
        cols=4,
        controls=controls,
        background=colors[0],
        foreground=colors[1],
        initial_focus=None,
    )
    layout.show(centered=True)


keys.extend([
    Key([mod], "F1", show_power_menu(keys=keys),
        desc="Qtile Keybindings"),
])

# ==========
# Groups
# ==========

groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = move focused window to group
            Key(
                [mod, shift],
                i.name,
                lazy.window.togroup(i.name, switch_group=False),
                desc="Move focused window to group {}".format(
                    i.name),
            ),
        ]
    )

# ==========
# Layouts and colors
# ==========

DoomOne = [
    ["#313640", "#313640"],  # bg
    ["#bbc2cf", "#bbc2cf"],  # fg
    ["#1c1f24", "#1c1f24"],  # color01
    ["#ff6c6b", "#ff6c6b"],  # color02
    ["#98be65", "#98be65"],  # color03
    ["#da8548", "#da8548"],  # color04
    ["#51afef", "#51afef"],  # color05
    ["#c678dd", "#c678dd"],  # color06
    ["#46d9ff", "#46d9ff"]  # color15
]
colors = DoomOne
layout_theme = {
    "border_width": 2,
    "margin": 8,
    "border_focus": colors[8],
    "border_normal": colors[0]
}

layouts = [
    layout.MonadTall(**layout_theme),
    layout.Max(border_width=0, margin=0),
]

# ==========
# Screens and widgets
# ==========

widget_defaults = dict(
    font="sans",
    fontsize=12,
    padding=3,
)
extension_defaults = widget_defaults.copy()


def getTopWidgets():
    topWidgets = [
        widget.GroupBox(
            disable_drag=True,
            toggle=False,
            foreground=colors[1],
            active=colors[1],
            inactive=colors[0],
            urgent_border=colors[3],
            urgent_text=colors[3],
            this_current_screen_border=colors[6],
            this_screen_border=colors[6],
            other_current_screen_border=colors[0],
            other_screen_border=colors[0]),
        widget.Sep(),
        widget.CurrentLayout(fmt=" {}", foreground=colors[1]),
        widget.Sep(),
        widget.WindowName(foreground=colors[1]),
        widget.Sep(),
        widget.TextBox(text="", foreground=colors[1]),
        widget.NetGraph(update_interval=3.0),
        widget.Sep(),
        widget.Volume(
            fmt=" {}",
            mouse_callbacks = {'Button3': lambda: qtile.cmd_spawn('pavucontrol')},
            foreground=colors[1]),
        widget.Sep(),
        widget.Clock(
            fmt=" {}",
            format="KW %V %a %d %b %Y %H:%M %Z",
            mouse_callbacks = {'Button3': lambda: qtile.cmd_spawn('evolution --component=calendar')},
            foreground=colors[1]),
    ]
    return topWidgets


def getBottomWidgets(idx: int):
    bottomWidgets = [
        widget.Chord(foreground=colors[3]),
        widget.Spacer(),
        widget.GenPollText(
            fmt=" {}",
            update_interval=300,
            func=lambda: subprocess.check_output(
                "printf $(uname -r)", shell=True, text=True),
            foreground=colors[1])
    ]
    if idx == 0:
        bottomWidgets.extend([
            widget.Sep(),
            widget.CheckUpdates(
                distro="Arch_checkupdates",
                fmt=" {}",
                update_interval=300,
                no_update_string="No updates",
                mouse_callbacks = {
                    'Button2': lambda: qtile.cmd_spawn([terminal,'bash','-c','pacman -Q | fzf --layout=reverse']),
                    'Button3': lambda: qtile.cmd_spawn([terminal,'bash','-c','checkupdates | fzf --layout=reverse'])
                },
                foreground=colors[1],
                colour_no_updates=colors[1],
                colour_have_updates=colors[3])
        ])
    bottomWidgets.extend([
        widget.Sep(),
        widget.CPU(
            fmt=" {}",
            format="{load_percent}%",
            update_interval=3.0,
            mouse_callbacks = {'Button3': lambda: qtile.cmd_spawn(terminal+' htop')},
            foreground=colors[1]),
        widget.Sep(),
        widget.Memory(
            fmt=" {}",
            format="{MemPercent}%",
            update_interval=3.0,
            mouse_callbacks = {'Button3': lambda: qtile.cmd_spawn(terminal+' htop')},
            foreground=colors[1]),
        widget.Sep(),
        widget.DF(
            fmt=" {}",
            partition="/",
            format="{r:.0f}%",
            visible_on_warn=False,
            mouse_callbacks = {'Button3': lambda: qtile.cmd_spawn(filemanager+' /')},
            update_interval=60.0,
            foreground=colors[1],
            warn_color=colors[3]),
        widget.Sep(),
        widget.DF(
            fmt=" {}",
            partition="/home",
            format="{r:.0f}%",
            visible_on_warn=False,
            mouse_callbacks = {'Button3': lambda: qtile.cmd_spawn(filemanager+' /home')},
            update_interval=60.0,
            foreground=colors[1],
            warn_color=colors[3]),
    ])
    temp_inputs = [x for x in glob.glob("/sys/class/hwmon/*/temp*_input")]
    if len(temp_inputs) > 0:
        bottomWidgets.extend([
            widget.Sep(),
            widget.ThermalSensor(
                fmt=" {}",
                format="{temp:.0f}{unit}",
                update_interval=3.0,
                foreground=colors[1],
                foreground_alert=colors[3]),
        ])
    bat_inputs = [x for x in glob.glob("/sys/class/power_supply/*/capacity")]
    if len(bat_inputs) > 0:
        bottomWidgets.extend([
            widget.Sep(),
            widget.Battery(
                fmt=" {}",
                update_interval=60.0,
                foreground=colors[1],
                low_foreground=colors[3])
        ])
    if qtile.core.name == "x11" and idx == 0:
        bottomWidgets.extend([
            widget.Sep(),
            widget.Systray()
        ])
    return bottomWidgets


screens = []
if qtile.core.name == "x11":
    dsp = display.Display()
    root = dsp.screen().root
    for idx, m in enumerate(root.xrandr_get_monitors().monitors):
        screens.append(Screen(
            top=bar.Bar(getTopWidgets(), 24),
            bottom=bar.Bar(getBottomWidgets(idx), 24)
        ))
elif qtile.core.name == "wayland":
    screens.append(Screen(
        top=bar.Bar(getTopWidgets(), 24),
        bottom=bar.Bar(getBottomWidgets(0), 24)
    ))


# ==========
# Mouse
# ==========

# Drag floating layouts.
mouse = [
    Drag(
        [mod],
        "Button1",
        # -> https://github.com/qtile/qtile/pull/3664
        # lazy.window.set_position_floating().when(when_floating=True),
        # lazy.window.set_position().when(when_floating=False),
        lazy.window.set_position_floating(),
        start=lazy.window.get_position()
    ),
    Drag(
        [mod],
        "Button3",
        # -> https://github.com/qtile/qtile/pull/3664
        # lazy.window.set_size_floating().when(when_floating=True),
        # lazy.window.set_size().when(when_floating=False),
        lazy.window.set_size_floating(),
        start=lazy.window.get_size()
    ),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

# ==========
# Hooks
# ==========

#
# bring floating windows to top
#
@hook.subscribe.float_change
@hook.subscribe.focus_change
def float_or_focus_changed():
    for window in qtile.current_group.windows:
        if window.floating:
            window.cmd_bring_to_front()

#
# startup programs
#
@hook.subscribe.startup_once
def start_once():
    processes = []
    processes_once = []
    if qtile.core.name == "x11":
        processes.extend([
            ['xset', '-b'],
            ['xset', '-dpms'],
            ['xset', 's', 'off']
        ])
        processes_once.extend([
            ['gammastep', '-l', 'manual:51:10', '-t', '6500:3500', '-b', '1:0.75', '-m', 'randr'],
            ['xautolock', '-time', '10', '-locker', 'slock_run', '-nowlocker', 'slock'],
            ['xeventbind', 'resolution', os.path.expanduser('~/.xeventbind')],
            ['flameshot'],
            ['dunst']
        ])
    elif qtile.core.name == "wayland":
        processes_once.extend([
            ['gammastep', '-l', 'manual:51:10', '-t', '6500:3500', '-b', '1:0.75', '-m', 'wayland'],
            ['flameshot'],
            ['dunst']
        ])
    
    # desktop autostart files
    processes.extend([
        ['dex', os.path.join(dp, f)] for dp, dn, fn in os.walk('/etc/xdg/autostart') for f in fn if f[-8:] == ".desktop"
    ])
    
    for p in processes:
        subprocess.Popen(p)
    for p in processes_once:
        if not process_up(p[0]):
            subprocess.Popen(p)

def process_up(proc = None):
    try:
        call = subprocess.check_output("pidof '{}'".format(proc), shell=True)
        return True
    except subprocess.CalledProcessError:
        return False

#
# random wallpapers
#
@hook.subscribe.startup
def set_wallpaper_and_timer():
    wallpapers = [
        os.path.join(dp, f) for dp, dn, fn in os.walk('/usr/share/backgrounds') for f in fn if f[-4:] in [".gif", ".png", ".jpg"]
    ]
    wallpaper = random.choice(wallpapers)
    for screen in qtile.screens:
        screen.cmd_set_wallpaper(wallpaper, "fill")
    qtile.call_later(2220, set_wallpaper_and_timer)


# ==========
# General
# ==========

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = False
bring_front_click = True
floats_kept_above = True
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(wm_class="qalculate-gtk"),  # Qalculate
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
