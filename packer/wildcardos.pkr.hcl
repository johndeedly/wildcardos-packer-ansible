packer {
  required_plugins {
    qemu = {
      source  = "github.com/hashicorp/qemu"
      version = "~> 1"
    }
    virtualbox = {
      source  = "github.com/hashicorp/virtualbox"
      version = "~> 1"
    }
    ansible = {
      source  = "github.com/hashicorp/ansible"
      version = "~> 1"
    }
  }
}


variable "yearmonthday" {
  type = string
}

variable "sound_driver" {
  type = string
}

variable "accel_graphics" {
  type = string
}

variable "cpu_cores" {
  type    = number
  default = 4
}

variable "memory" {
  type    = number
  default = 3072
}

variable "encryption" {
  type    = bool
  default = false
}

variable "dualboot" {
  type    = bool
  default = false
}

variable "headless" {
  type    = bool
  default = true
}

variable "pxe" {
  type    = bool
  default = false
}

variable "stage" {
  type    = string
  default = "graphical"
}

variable "configuration" {
  type    = list(string)
  default = [
    "arch",
    #"ubuntu",
    #"rocky",
    "verbose",
    "build_archiso",
    #"target_host",
    "target_guest",
    #"target_container",
    #"target_nspawn",
    "bootstrap",
    "graphical",
    "qtile",
    #"encryption",
    #"dualboot",
    #"pxeboot",
    #"import",
    #"export",
  ]
}

variable "extra_vars" {
  type    = map(string)
  default = {
    "partitioning_install_device_node" = "/dev/vda",
    "partitioning_root_mount_point" = "/var/lib/machines/archlinux",
    "base_bootable_ubuntu_release" = "mantic"
  }
}


source "null" "default" {
  ssh_host     = "127.0.0.1"
  ssh_username = "root"
  ssh_password = "packer-build-passwd"
  ssh_timeout  = "30s"
}


source "qemu" "default" {
  shutdown_command   = "/sbin/poweroff"
  cd_files           = ["CIDATA/*"]
  cd_label           = "CIDATA"
  disk_size          = 524288
  memory             = var.memory
  format             = "qcow2"
  accelerator        = "kvm"
  disk_discard       = "unmap"
  disk_detect_zeroes = "unmap"
  disk_interface     = "virtio"
  disk_compression   = false
  skip_compaction    = true
  net_device         = "virtio-net"
  vga                = "virtio"
  machine_type       = "q35"
  cpu_model          = "host"
  vtpm               = true
  tpm_device_type    = "tpm-tis"
  efi_boot           = true
  efi_firmware_code  = "/usr/share/OVMF/x64/OVMF_CODE.secboot.4m.fd"
  efi_firmware_vars  = "/usr/share/OVMF/x64/OVMF_VARS.4m.fd"
  sockets            = 1
  cores              = var.cpu_cores
  threads            = 1
  qemuargs           = [["-rtc", "base=utc,clock=host"], ["-usbdevice", "mouse"], ["-usbdevice", "keyboard"], ["-virtfs", "local,path=output,mount_tag=host.0,security_model=mapped,id=host.0"]]
  headless           = var.headless
  iso_checksum       = "none"
  iso_url            = "archlinux-${var.yearmonthday}-x86_64.iso"
  output_directory   = "output/${var.stage}"
  ssh_username       = "root"
  ssh_password       = "packer-build-passwd"
  ssh_timeout        = "10m"
  vm_name            = "wildcardos-${var.stage}-${var.yearmonthday}-x86_64.qcow2"
}


source "virtualbox-iso" "default" {
  shutdown_command         = "/sbin/poweroff"
  cd_files                 = ["CIDATA/*"]
  cd_label                 = "CIDATA"
  disk_size                = 524288
  memory                   = var.memory
  format                   = "ovf"
  guest_additions_mode     = "disable"
  guest_os_type            = "ArchLinux_64"
  hard_drive_discard       = true
  hard_drive_interface     = "sata"
  hard_drive_nonrotational = true
  headless                 = var.headless
  iso_checksum             = "none"
  iso_interface            = "sata"
  iso_url                  = "archlinux-${var.yearmonthday}-x86_64.iso"
  output_directory         = "output/${var.stage}"
  output_filename          = "../wildcardos-${var.stage}-${var.yearmonthday}-x86_64"
  ssh_username             = "root"
  ssh_password             = "packer-build-passwd"
  ssh_timeout              = "10m"
  vboxmanage               = [["modifyvm", "{{ .Name }}", "--chipset", "ich9", "--firmware", "efi", "--cpus", "${var.cpu_cores}", "--audio-driver", "${var.sound_driver}", "--audio-out", "on", "--audio-enabled", "on", "--usb", "on", "--usb-xhci", "on", "--clipboard", "hosttoguest", "--draganddrop", "hosttoguest", "--graphicscontroller", "vmsvga", "--acpi", "on", "--ioapic", "on", "--apic", "on", "--accelerate3d", "${var.accel_graphics}", "--accelerate2dvideo", "on", "--vram", "128", "--pae", "on", "--nested-hw-virt", "on", "--paravirtprovider", "kvm", "--hpet", "on", "--hwvirtex", "on", "--largepages", "on", "--vtxvpid", "on", "--vtxux", "on", "--biosbootmenu", "messageandmenu", "--rtcuseutc", "on", "--nictype1", "virtio", "--macaddress1", "auto"]]
  vboxmanage_post          = [["modifyvm", "{{ .Name }}", "--macaddress1", "auto"]]
  vm_name                  = "wildcardos-${var.stage}-${var.yearmonthday}-x86_64"
}


build {
  sources = ["source.null.default", "source.qemu.default", "source.virtualbox-iso.default"]

  # mount the external output folder as share
  provisioner "shell" {
    inline = [
      "mount --mkdir -t 9p -o trans=virtio,version=9p2000.L,rw host.0 /share || true"
    ]
    only   = ["qemu.default"]
  }

  # remount copy on write space
  provisioner "shell" {
    inline = [
      "mount -o remount,size=75% /run/archiso/cowspace || true"
    ]
  }

  # make the journal log persistent on ramfs
  provisioner "shell" {
    inline = [
      "mkdir -p /var/log/journal",
      "systemd-tmpfiles --create --prefix /var/log/journal",
      "systemctl restart systemd-journald"
    ]
  }

  # wait for pacman keyring init to be done
  provisioner "shell" {
    inline = [
      "while ! systemctl show pacman-init.service | grep SubState=exited; do",
      "  systemctl --no-pager status -n0 pacman-init.service || true",
      "  sleep 5",
      "done",
    ]
  }

  # prepare ansible in archiso environment
  provisioner "shell" {
    inline = [
      "pacman -Syy --needed --noconfirm --color=auto ansible python-pip python-setuptools python-wheel python-lxml python-passlib"
    ]
  }

  # prepare nspawn environment to allow ufw firewall configuration
  provisioner "shell" {
    inline = [
      "modprobe iptable_filter",
      "modprobe ip6table_filter"
    ]
  }

  # create the ansible build folder
  provisioner "shell" {
    inline = [
      "mkdir -m755 /install",
    ]
  }
  
  # trailing slash: content of ansible is copied to the /install folder
  provisioner "file" {
    source = "ansible/"
    destination = "/install"
  }

  provisioner "shell" {
    inline = [
      "pushd /install",
      "ansible-playbook site.yml --tags ${join(",", var.configuration)} --extra-vars '${jsonencode(var.extra_vars)}' -c local -i inventory.yml",
      "popd"
    ]
    environment_vars = [
      "ANSIBLE_FORCE_COLOR=1",
      "PYTHONUNBUFFERED=1"
    ]
  }

  provisioner "shell-local" {
    inline = [<<EOS
tee output/${var.stage}/wildcardos-${var.stage}-${var.yearmonthday}-x86_64.run.sh <<EOF
#!/usr/bin/env bash
trap "trap - SIGTERM && kill -- -\$\$" SIGINT SIGTERM EXIT
mkdir -p "/tmp/swtpm.0" "share"
/usr/bin/swtpm socket --tpm2 --tpmstate dir="/tmp/swtpm.0" --ctrl type=unixio,path="/tmp/swtpm.0/vtpm.sock" &
/usr/bin/qemu-system-x86_64 \\
  -name wildcardos-${var.stage}-${var.yearmonthday}-x86_64 \\
  -machine type=q35,accel=kvm \\
  -vga virtio \\
  -cpu host \\
  -drive file=wildcardos-${var.stage}-${var.yearmonthday}-x86_64.qcow2,if=virtio,cache=writeback,discard=unmap,detect-zeroes=unmap,format=qcow2 \\
  -device tpm-tis,tpmdev=tpm0 -tpmdev emulator,id=tpm0,chardev=vtpm -chardev socket,id=vtpm,path=/tmp/swtpm.0/vtpm.sock \\
  -drive file=/usr/share/OVMF/x64/OVMF_CODE.secboot.4m.fd,if=pflash,unit=0,format=raw,readonly=on \\
  -drive file=efivars.fd,if=pflash,unit=1,format=raw \\
  -smp ${var.cpu_cores},sockets=1,cores=${var.cpu_cores},maxcpus=${var.cpu_cores} -m ${var.memory}M \\
  -netdev user,id=user.0 -device virtio-net,netdev=user.0 \\
  -audio driver=pa,model=hda,id=snd0 -device hda-output,audiodev=snd0 \\
  -virtfs local,path=share,mount_tag=host.0,security_model=mapped,id=host.0 \\
  -usbdevice mouse -usbdevice keyboard \\
  -rtc base=utc,clock=host
EOF
# -display none, -daemonize, hostfwd=::12345-:22 for running as a daemonized server
chmod +x output/${var.stage}/wildcardos-${var.stage}-${var.yearmonthday}-x86_64.run.sh
EOS
    ]
    only_on = ["linux"]
    only    = ["qemu.default"]
  }
}
